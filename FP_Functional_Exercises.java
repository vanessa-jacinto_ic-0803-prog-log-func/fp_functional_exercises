import java.util.List;

public class FP_Functional_Exercises{
	
	public static void main(String[] args ){
		List<Integer> numbers= List.of(12, 9, 13, 4, 6, 2, 4, 12,15);
		List<String> courses= List.of("Spring","Spring Boot", "API", "Microservices",
		 "AWS", "PCF", "Azure", "Docker", "Kubernetes");
			System.out.println("\n Numeros impares del ejercicio 1  ");
	impares(numbers);
System.out.println("Otra solución ");
numbers.stream()
		.filter(FP_Functional_Exercises::isEv)
		.forEach(numer -> System.out.println(" " + numer));
	System.out.println("\n Impresión de todas Palabras del ejercicio 2  ");
	palabras(courses);
	System.out.println("\n Impresión de las Palabras que contienen la palabra Spring del ejercicio 3  ");
	spring3(courses);
	System.out.println("\n Palabras que tienen mas de Longitud 4   del ejercicio 4  ");
	palabra4(courses);
	System.out.println("\n Numeros impares al cuadrado del ejercicio 5  ");
		cuadradoimpares(numbers);
	System.out.println("\n Longitud de cada palabra del ejercicio 6  ");
		spring6(courses);
	

	


	}
	private static void print(int number){
 	System.out.println(number + ",");
 }

 
private static void print2(String leters){
 	System.out.println(leters + " ");
 }
 
 private static boolean isEven(int number){
 	return (number % 2 == 0 + 1);
 
 
}
//otra posible solucion
 private static boolean isEv(int number){
 	return (number % 2 != 0 );
 
 
}
	private static void impares(List<Integer>numbers){
	numbers.stream()
		.filter(FP_Functional_Exercises::isEven)
		.forEach(FP_Functional_Exercises::print);
			System.out.println(" ");
	}

			


private static void cuadradoimpares(List<Integer>numbers){
	numbers.stream()
		.filter(number -> number % 2 == 0 + 1)
		.map(number -> number * number)
		.forEach(FP_Functional_Exercises::print);
			System.out.println(" ");
	}

	private static void palabras(List<String>courses){
	courses.stream()
		.forEach(le -> System.out.println(le));
			
	}

	private static void palabra4(List<String>courses){
	courses.stream()
	.filter(x -> x.length()> 4)
		.forEach(System.out::println);
			
	}
	private static void spring3(List<String>courses){
	courses.stream()
		.filter(x -> x.contains("Spring"))
		.forEach(System.out::println);
			
	}

	private static void spring6(List<String>courses){
	courses.stream()
	
		.forEach(le -> System.out.println("La cadena es: " + le + " tiene la longitud de: " + le.length()));
			
	}
	
	
	
}